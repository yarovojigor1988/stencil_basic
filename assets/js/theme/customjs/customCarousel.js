import $ from 'jquery'
import 'slick-carousel';
import PageManager from '../page-manager'

export default class CustomDemo extends PageManager {
    onReady () {
        console.log('CONNECTED!!!');
        console.log($('#customSlickId'));
        console.log(document.getElementById('customSlickId'));

        $('#customSlickId').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
        });
    }
}